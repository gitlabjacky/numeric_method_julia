# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,jl:percent
#     text_representation:
#       extension: .jl
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Julia 1.7.2
#     language: julia
#     name: julia-1.7
# ---

# %% [markdown]
# ## Consider the following functions for optimization:
#
# - $g(x) = -3x^3 + 9x^2 + 2x$
#
#
# - $h(x,y) =  -(1.5 - x)^2 - 100(y - x^2)^2$
#
#
# - $k(x,y) = - (x + 2y - 7)^2 - (2x + y -5)^2$
#
#
#
#
# ### Write a Julia program to implement Newton's method of finding the maximizer of a uni-variate function. The program should meet the following requirements:
#
#
# - The inputs should at least contain the following items:
#
# ```julia
# function newton_max_uni_a(f::Function, f′::Function, f′′::Function, x0, ϵ, maxit)
#     ...
# end
# ```
#    where `f`, `f′`, and `f′′` are the analytic equations of the objective function and its first and second derivatives, respectively; `x0` is the initial value, `ϵ` is the tolerance of convergence criterion, and `maxit` is the maximum number of iterations.
#
#
# - Use the change in the solution as the basis to check convergence.
#
#
# - The program should be able to check whether the found solution is a maximizer or a minimizer (hint: check the 2nd order condition). If it is a minimizer instead of a maximizer, a warning should be issued.
#
#
# - The program should print at least the following information: the solution, the number of iterations, and actual ϵ. If the number of iterations equals `maxit` (meaning, the estimation may not have converged), a warning should be issue
#
#

# %%
function output(typ, sol, it, ϵ; l=40)  # format the output
    sol, it, ϵ = string(sol), string(it), string(ϵ)
    v = l - 15
    v1, v2, v3, v4 = div((v - 7), 2), div(v - length(sol), 2), div(v - length(it), 2),
                     div(v - length(ϵ), 2)
    s1 = (typ == "max" ? "Maximum" : "minimum")
    s1, s2, s3, s4 = " " ^ v1 * s1 * " "^v1, " " ^ v2 * sol * " "^v2, " " ^ v3 * it * " "^v3,
                     " " ^ v4 *  ϵ * " "^v4
    k1 = (v - length(sol)) % 2 == 0 ? 0 : 1
    k2 = (v - length(it)) % 2 == 0 ? 0 : 1
    k3 = (v - length(ϵ)) % 2 == 0 ? 0 : 1
    println("-"^l)
    println("|" * "    type    " * "|" * s1 * "|")
    println("-"^l)
    println("|" * "  solution  " * "|" * s2 * " "^k1 * "|")
    println("-"^l)
    println("|" * " iterateNum " * "|" * s3 * " "^k2 * "|")
    println("-"^l)
    println("|" * "  actual_ϵ  " * "|" * s4 * " "^k3 * "|")
    println("-"^l)
end

# %%
function newton_max_uni_a(f::Function, f1::Function, f2::Function, x0, ϵ, maxit)
    x, x_, lr = x0, x0, 1  # (use x_ to store the previous solution x_{i-1})
    for i = 1:maxit+1  # total round = first round + max iterations
        df1, df2 = f1(x), f2(x)
        if df1 == 0 && df2 < 0
            output("max", x, i-1, abs(x-x_)); return
        elseif df1 == 0 && df2 > 0
            output("min", x, i-1, abs(x-x_)); return
        elseif df2 == 0
            mu = 1e-1
            x = df1 != 0 ? (df1 > 0 ? x + mu : x - mu) : (f1(x_) > 0 ? (x + mu) : (x - mu))
            df1, df2 = f1(x), f2(x)
        elseif i != 1 && abs(x - x_) < ϵ
            actual_ϵ = abs(x - x_)
            df2 < 0 ? output("max", x, i-1, actual_ϵ) : output("min", x, i-1, actual_ϵ)
            return
        end
        i != maxit ? (x_ = x; x = x - lr*(df1/df2)) : println("do not find a local Maximum or minimum...")
    end
end

# %% [markdown]
# #### Use the program to estimate $g(x)$. Use 0.99 and 1.01 as initial values, respectively. Do they converge to the same solution? Does the result indicate that initial values are important for Newton's methods? In light of this exercise, do you think the Newton's method is a local method or a global method?
#
# #### answer
# 1. They do not converge to the same solution
# 2. Since the answer differ, the initial point has a significant influence
# 3. Since the process terminates when finding a minimum or maximum. It's a local method

# %%
g(x) = -3x^3 + 9x^2 + 2x
g1(x) = -9x^2 + 18x + 2
g2(x) = -18x + 18
newton_max_uni_a(g, g1, g2, 0.99, 1e-8, 100)

# %%
newton_max_uni_a(g, g1, g2, 1.01, 1e-8,  100)

# %% [markdown]
# #### Use the program to estimate $g(x)$. Use 1.0 as the initial value.
#
#  - Why this initial value does not work for the program? Do you think that the problem arises because the function has an *inflection point* at $x=1.0$?
#
#  - The problem indicates that the program would fail when the function is not smoothly increasing or increasing. Modify your program such that it could get out of the situation and move on. When you implement the rescue plan, be sure to make use of the fact that the purpose of the program is to **maximize** a function.
#
# #### answer
#  1. f''(0.99) > 0 and f''(1.01) < 0 
#  2. f'(1.) $\neq$ 0 and  f''(1.) = 0, it's a non-stationary inflection point

# %%
newton_max_uni_a(g, g1, g2, 1., 1e-8, 13)

# %%
using Plots
import Latexify: latexify
label = latexify(:(-3x^3+9x^2+2x))
plot(g, xlim=[-2, 3], label="g(x)= "*label)

# %% [markdown]
# #### Modify the above program to meet the following specifications
#
# - Instead of requiring users to input analytic forms of the function's 1st and 2nd derivatives, Use Julia's `ForwardDiff` to do it automatically. The program should look like the following where the 1st and 2nd derivatives are calculated using `ForwardDiff` within the program. Estimate $g(x)$ using this program.
#
# ```julia
# using ForwardDiff
# function newton_max_uni_b(f::Function, x0, ϵ, imax)
#     ....
# end
# ```
# Hint: `Forward.derivative()` is your friend. You need to find out how to use it to calculate the 2nd derivative.

# %%
import ForwardDiff: gradient, hessian
function newton_max_uni_b(f::Function, x0, ϵ, maxit)
    F(y::Vector) = f(y[1])
    f1(y::Vector) = gradient(F, y)
    f2(y::Vector) = hessian(F, y)
    x, x_, lr = x0, x0, 1
    for i = 1:maxit+1
        df1, df2 = f1([x])[1], f2([x])[1, 1]
        if df1 == 0 && df2 < 0
            output("max", x, i-1, abs(x-x_)); return
        elseif df1 == 0 && df2 > 0
            output("min", x, i-1, abs(x-x_)); return
        elseif df2 == 0
            mu = 1e-1
            x = df1 != 0 ? (df1 > 0 ? x + mu : x - mu) : (f1([x_]) > 0 ? (x + mu) : (x - mu))
            df1, df2 = f1([x])[1], f2([x])[1, 1]
        elseif i != 1 && abs(x - x_) < ϵ
            actual_ϵ = abs(x - x_)
            df2 < 0 ? output("max", x, i-1, actual_ϵ) : output("min", x, i-1, actual_ϵ)
            return
        end
        i != maxit+1 ? (x_ = x; x = x - lr*(df1/df2)) : println("do not find a local Maximum or minimum...")
    end
end

# %%
newton_max_uni_b(g, 0.99, 1e-8, 100)

# %%
newton_max_uni_b(g, 1.01, 1e-8, 100)

# %%
newton_max_uni_b(g, 1., 1e-8, 100)

# %% [markdown]
# ### Write a Julia program to implement Newton's method of finding the maximizers of a multi-variate function. The program should meet the following requirements:
#
#
# - The inputs should at least contain the following items:
#
# ```julia
# function newton_max_a(f::Function, init, ϵ, maxit)
#    ...
# end
# ```
#    where `f` is the objective function, `init` is the vector of initial values, `ϵ` is the tolerance of convergence criterion, and `maxit` is the maximum number of iterations.
#
# - Use `ForwardDiff` to calculate the gradient vector and the Hessian matrix.
#   - Hint: `ForwardDiff.gradient()` and `ForwardDiff.hessian()`.
#
#
# - Use the norm of the gradient as the basis to check convergence.
#   - Hint: Google how to calculate the norm of a vector in Julia.
#
#
# - The program should be able to check whether the found solution is a maximizer or a minimizer (hint: check whether the Hessian is positive definite or negative definite). If it is a minimizer instead of a maximizer, a warning should be issued.
#   - Hint: Google how to check a matrix's definite using Julia.
#
#
# - The program should print at least the following information: the solution, the number of iterations, and actual ϵ. If the number of iterations equals `maxit` (meaning, the estimation may not have converged), a warning should be issued.

# %%
using LinearAlgebra
function newton_max_a(f::Function, init, ϵ, maxit)
    f1(y::Vector) = gradient(f, y)
    f2(y::Vector) =  hessian(f, y)
    x, x_, lr = init, init, 1
    for i = 1:maxit+1
        df1, df2 = f1(x), f2(x)
        df1L, pd = norm(df1), isposdef(df2)
        if df1L == 0 && isposdef(-df2)
            output("max", x, i-1, norm(x .- x_), l=100); return
        elseif df1L == 0 && pd
            output("min", x, i-1, norm(x .- x_), l=100); return
        elseif det(df2) == 0
            mu = 1e-1
            x = df1L != 0 ? (df1L > 0 ? x .+ mu : x .- mu) : (norm(f1([x_])) > 0 ? (x .+ mu) : (x .- mu))
            df1, df2 = f1(x), f2(x)
        elseif i != 1 && norm(x .- x_) < ϵ
            actual_ϵ = norm(x .- x_)
            pd ? output("min", x, i-1, actual_ϵ, l=100) : output("max", x, i-1, actual_ϵ, l=100)
            return
        end
        i != maxit+1 ? (x_ = x; x = x .- lr.*(inv(df2)*df1)) : println("do not find a local Maximum or minimum...")
    end
end

# %% [markdown]
# #### Use the program to estimate $h(x,y)$. Hint: Code the function as `h(x) = -(1.5 - x[1])^2 - 100*(x[2] - x[1]^2)^2` where `x` is a vector.

# %%
h(x::Vector) =  -(1.5 - x[1])^2 - 100(x[2] - x[1]^2)^2
newton_max_a(h, [0.99, 1.01], 1e-8, 100)

# %%
default(size=(600,600), fc=:heat)
x, y = -2.:0.1:2., -1.:0.1:3.
z = Surface((x,y)->h([x,y]), x, y)
surface(x,y,z, linealpha = 0.5)

# %% [markdown]
# #### Use the program to estimate $k(x,y)$. Hint: Code the function as `k(x) = - (x[1] + 2*x[2] - 7)^2 - (2*x[1] + x[2] -5)^2` where `x` is a vector. The estimation should converge in 1 iteration.

# %%
k(x::Vector) = - (x[1] + 2x[2] - 7)^2 - (2x[1] + x[2] -5)^2
newton_max_a(k, [0.99, 1.01], 1e-8, 100)

# %%
default(size=(600,600), fc=:heat)
x, y = -1.:0.1:2., -1.:0.1:4.
z = Surface((x,y)->k([x,y]), x, y)
surface(x,y,z, linealpha = 1.5)

# %% [markdown]
# #### Use the program to estimate $g(x)$. Hint: Code the function as `g(x) = -3*x[1]^3 + 9*x[1]^2 + 2*x[1]` where `x` is a vector.

# %%
g(x::Vector) = -3x[1]^3 + 9x[1]^2 + 2x[1]
newton_max_a(g, [0.99], 1e-8, 100)

# %%
newton_max_a(g, [1.01], 1e-8, 100)

# %%
newton_max_a(g, [1.], 1e-8, 100)

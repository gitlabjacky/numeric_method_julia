module quaMCM_mod
    export prime_test

    include("GFT_param.jl")
    using HaltonSequences: Halton
    using .GFT_param_mod: rlb, rlb_aut, domain_trans

    function prime_test(prime::Int64, n::Int64)
        f = domain_trans(rlb, 1.)
        f_aut = domain_trans(rlb_aut, 1.)
        GFT = zeros(n)
        for i = 1:n
            hal_seq = Halton(prime, length=i)
            W = sum(f.(hal_seq)) / i
            W_aut = sum(f_aut.(hal_seq)) / i
            GFT[i] = W / W_aut
        end
        return GFT
    end
end  # end of the module
